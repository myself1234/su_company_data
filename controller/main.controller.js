sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/Fragment',
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast',
    'sap/ui/unified/FileUploaderParameter',
    'sap/m/Dialog',
    "sap/m/MessageBox",
    'sap/m/Text',
    'sap/m/Button',
    'sap/m/List',
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator',
    'sap/ui/model/FilterType',
    'sap/m/StandardListItem',
    './propDetails.formatter',
    'sap/m/MessagePopover',
    'sap/m/MessagePopoverItem'
], function(
    jQuery,
    Fragment,
    Controller,
    JSONModel,
    MessageToast,
    fileUpload,
    Dialog,
    MessageBox,
    Text,
    Button,
    List,
    Filter,
    FilterOperator,
    FilterType,
    StandardListItem,
    formatter,
    MessagePopover,
    MessagePopoverItem) {

    "use strict";

    var showHeaderForm = false;
    var showHeaderView = true;
    var comsTableModel = null;
    var productCatTableModel = null;
    var productTreeModel = null;
    var origProdTreeModel = null;
    var countriesModel = null;
    var regionsModel = null;
    var countryRegionsModel = null;
    var taxTypeModel = null;
    var currencyTypeModel = null;
    var masterDetailsModel = null;
    var objectControllerModel = null;
    var prodCatCount = null;
    var userModel = null;
    var userName = null;
    var aliasModel = null;
    var faxTableModel = null;
    var taxTableModel = null;
    var emailTableModel = null;
    var supplierId = null;
    var prodCatsModel = null;
    var employeeModel = null;
    var selectedTreeModelCatObj = [];
    var userAlias = null;
    var userName = null;
    var messagesData = {};
    var Messages = new JSONModel(messagesData);
    var oErrorIcon = null;


    var oMessageTemplate = new MessagePopoverItem({
      type: '{type}',
      title: '{title}'
    });

    var oMessagePopover = new MessagePopover({
      items: {
        path: '/messages',
        template: oMessageTemplate
      }
    });

    return Controller.extend("srm_compData.controller.main", {
        userUrl: "/sap/bc/ui2/start_up",
        aliasUrl: "/sap/opu/odata/BSHB2B/SU_USER_SRV/",
        servUrl: "/sap/opu/odata/BSHD/SU_MSTR_DATA_SRV/",
        prodCatsUrl: "/sap/opu/odata/BSHD/SU_PROD_CATEGORY_SRV/",
        contactServUrl: "/sap/opu/odata/BSHB2B/SU_CONTACTS_SRV/",

        formatter: formatter,
        ressDialog: null,
        fixedSizeDialog: null,
        resizableDialog: null,
        draggableDialog: null,
        escapePreventDialog: null,
        confirmEscapePreventDialog: null,

        onInit: function(oEvent) {
            var oController = this;

            this.getView().setBusyIndicatorDelay(10);
            this.getView().setBusy(true);

            oErrorIcon = this.getView().byId("errorIcon");

            oErrorIcon.onAfterRendering = function(){
              oMessagePopover.openBy(oErrorIcon);
            }

            //Messages model
            messagesData.messagesLength = 0;
            messagesData.messages = [];
            Messages.setData(messagesData);
            this.getView().setModel(Messages, "Messages");
            oMessagePopover.setModel(Messages);

            //Metadata Service
            var oMetadataModel = new sap.ui.model.odata.v2.ODataModel(this.servUrl, true);
            this.getView().setModel(oMetadataModel, "oMetadataModel");
            sap.ui.getCore().setModel(oMetadataModel, "oMetadataModel");


            this._createProductsModel();

            this.getUserNameData();

            var aliasService = new sap.ui.model.odata.ODataModel(this.aliasUrl);
            var aliasServiceEntity = "SupplierVendorSet?$filter=Bname eq '" + userName + "'";

            this.getDataPromise(aliasService, aliasServiceEntity).then(function(data) {

                var key = null;
                aliasModel = new JSONModel(data);

                for (key in data.results) {
                    if (data.results[key].Werks === "0000") {
                        userAlias = data.results[key].SrmSystemAlias;
                        userName = data.results[key].Bname;
                        supplierId = data.results[key].Lifnr;
                        break;
                    }
                }

                oController.sSupplierId = supplierId;


                //GET MASTER DATA
                oController.loadCompanyDetails(supplierId);

                //GET COUNTRIES DATA
                var countriesService = new sap.ui.model.odata.ODataModel(oController.servUrl, true);
                var countriesDetailsreadEnity = "DropDownSet?$filter= Input eq 'CN'";
                oController.getDataPromise(countriesService, countriesDetailsreadEnity).then(function(data) {
                    countriesModel = new JSONModel(data);
                    oController.setCountries();
                }).catch(function(err) { oController.handleGetAjaxFail(err); });


                //GET REGIONS DATA
                var regionsService = new sap.ui.model.odata.ODataModel(oController.servUrl, true);
                var regionsDetailsreadEnity = "DropDownSet?$filter= Input eq 'RG'";
                oController.getDataPromise(regionsService, regionsDetailsreadEnity).then(function(data) {
                    regionsModel = new JSONModel(data);
                    oController.setRegions();
                }).catch(function(err) { oController.handleGetAjaxFail(err); });

                //GET TAX DATA
                var taxTypeService = new sap.ui.model.odata.ODataModel(oController.servUrl, true);
                var taxTypereadEntity = "DropDownSet?$filter= Input eq 'TX'";
                oController.getDataPromise(taxTypeService, taxTypereadEntity).then(function(data) {;
                    taxTypeModel = new JSONModel(data);
                    oController.setTax();
                }).catch(function(err) { oController.handleGetAjaxFail(err); });

                //GET CURRENCY DATA
                var currencyTypeService = new sap.ui.model.odata.ODataModel(oController.servUrl, true);
                var currencyTypereadEntity = "DropDownSet?$filter= Input eq 'CR'";
                oController.getDataPromise(currencyTypeService, currencyTypereadEntity).then(function(data) {;
                    currencyTypeModel = new JSONModel(data);
                    oController.setCurrency();
                }).catch(function(err) { oController.handleGetAjaxFail(err); });

                //GET PROD CATS DATA
                var prodcatsService = new sap.ui.model.odata.ODataModel(oController.prodCatsUrl, true);
                var prodCatsEntity = "GetListSet?$filter=Supplier eq '" + supplierId + "'";
                oController.getDataPromise(prodcatsService, prodCatsEntity).then(function(data) {
                    prodCatsModel = new JSONModel(data);
                    oController.setProductCatsTable();
                }).catch(function(err) { oController.handleGetAjaxFail(err); });

                //EMPLOYEE CONTACT
                var employeeService = new sap.ui.model.odata.ODataModel(oController.contactServUrl, true);
                var employeeReadEntity = "ContactDataSet?$filter=Username eq '" + userName + "'";
                oController.getDataPromise(employeeService, employeeReadEntity).then(function(data) {
                    employeeModel = new JSONModel(data);
                    oController.setEmployeeContactTable();
                }).catch(function(err) { oController.handleGetAjaxFail(err); });


            });

        },


        loadCompanyDetails: function(sSupplierId) {
            var oController = this;

            if (!sSupplierId || sSupplierId=="") return;

            //GET PROFILE IMAGE AND BIND
            oController.setProfileImage();

            //GET MASTER DATA
            var masterDetailService = new sap.ui.model.odata.ODataModel(oController.servUrl);
            var masterDetailParams = "?$expand=NavCompanyTax,NavCompanyFax,NavCompanyEmail,NavCompanyPhone,NavCompanyBank";
            var masterDetailsreadEntity = "CompanyDataSet(Supplier='" + sSupplierId + "')" + masterDetailParams;

            oController.getDataPromise(masterDetailService, masterDetailsreadEntity).then(function(data) {
                //process PO Box without number
                if(data.PoWONo == "true") data.PoWONo = true;
                else data.PoWONo = false;

                if (!masterDetailsModel) {
                    masterDetailsModel = new JSONModel();
                }
                masterDetailsModel.setData(data);

                if (!objectControllerModel) {
                    var oData = {
                        Active      : false,
                        WorkflowAct : false
                    };
                    objectControllerModel = new JSONModel(oData);
                    oController.getView().setModel(objectControllerModel, "ObjectController")
                }

                if(masterDetailsModel.getProperty("/WorkflowAct") == "true"){
                    objectControllerModel.setProperty("/WorkflowAct", true);
                }else{
                    objectControllerModel.setProperty("/WorkflowAct", false);
                }

                oController.setMasterData();
                oController.getView().setBusy(false);

            }).catch(function(err) { oController.handleGetAjaxFail(err); });
        },


        saveCompanyDetails: function() {
            var oView = this.getView();
            oView.setBusy(true);
            oErrorIcon.setVisible(false);
            var data = this.getView().getModel().getData();
            var payload = {};
            var self = this;
            payload.Supplier = supplierId;
            payload.Name1 = data.Name1;
            payload.Name2 = data.Name2;
            payload.Langu = data.Langu;
            payload.WebAddress = data.WebAddress;
            payload.DUNSNumber = data.DUNSNumber;
            payload.Taxjurcode = data.Taxjurcode;
            payload.Currency = data.Currency;
            payload.City = data.City;
            payload.District = data.District;
            payload.Country = data.Country;
            payload.Region = data.Region;
            payload.PostlCod1 = data.PostlCod1;
            payload.PostlCod2 = data.PostlCod2;
            payload.PostlCod3 = data.PostlCod3;
            payload.Street = data.Street;
            payload.HouseNo = data.HouseNo;
            payload.StrSuppl1 = data.StrSuppl1;
            payload.StrSuppl3 = data.StrSuppl3;
            payload.Building = data.Building;
            payload.Floor = data.Floor;
            payload.RoomNo = data.RoomNo;
            payload.PoBox = data.PoBox;
            payload.PoboxCtry = data.PoboxCtry;
            payload.PoBoxCit = data.PoBoxCit;
            payload.PoWONo  = data.PoWONo;
            //payload.SAP__Origin = "SUPPL_SRM";
            var i = 0;
            var phone = [];
            data.NavCompanyPhone.results.forEach(function(item) {
                delete(item.__metadata);
                if (item.StdNo=="true")  item.StdNo = true;
                if (item.StdNo=="false") item.StdNo = false;
                phone[i] = item;
                i++;
            })
            var j = 0;
            var fax = [];
            data.NavCompanyFax.results.forEach(function(item) {
                delete(item.__metadata);
                if (item.StdNo=="true")  item.StdNo = true;
                if (item.StdNo=="false") item.StdNo = false;
                fax[j] = item;
                j++;
            })
            var k = 0;
            var email = [];
            data.NavCompanyEmail.results.forEach(function(item) {
                delete(item.__metadata);
                if (item.StdNo=="true")  item.StdNo = true;
                if (item.StdNo=="false") item.StdNo = false;
                email[k] = item;
                k++;
            })
            var l = 0;
            var tax = [];
            data.NavCompanyTax.results.forEach(function(item) {
                delete(item.__metadata);
                tax[l] = item;
                l++;
            })
            if (email[0]) {
                payload.NavCompanyEmail = email;
            } else {
                var NavCompanyEmail = [];
                NavCompanyEmail[0] = {
                    RecordGuid: "",
                    EMail: ""
                };
                payload.NavCompanyEmail = NavCompanyEmail;
            }
            if (fax[0]) {
                payload.NavCompanyFax = fax;
            } else {
                var NavCompanyFax = [];
                NavCompanyFax[0] = {
                    Country: "",
                    Extension: "",
                    Fax: "",
                    RecordGuid: ""
                };

                payload.NavCompanyFax = NavCompanyFax;
            }
            if (phone[0]) {
                payload.NavCompanyPhone = phone;
            } else {
                var NavCompanyPhone = [];
                NavCompanyPhone[0] = {
                    RecordGuid: "",
                    Telephone: "",
                    Country: "",
                    Extension: ""
                };

                payload.NavCompanyPhone = NavCompanyPhone;
            }

            if (tax[0]) {
                payload.NavCompanyTax = tax;
            } else {
                var NavCompanyTax = [];
                NavCompanyTax[0] = {
                    TaxType: "",
                    TaxNumber: ""
                };

                payload.NavCompanyTax = NavCompanyTax;
            }

            console.log(payload);

            var payload1 = {};


            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var readReqURL = "/sap/opu/odata/BSHD/SU_MSTR_DATA_SRV/";
            var oModel = new sap.ui.model.odata.ODataModel(readReqURL, true);

            oModel.create("/CompanyDataSet", payload, {
                success: function(data, oResponse) {
                    oErrorIcon.setVisible(false);
                    MessageBox.success(bundle.getText("changeRequestCreated"), {
                          onClose: function(oAction){
                              if(oAction === MessageBox.Action.OK){
                                location.reload();
                              }
                            }
                    });

                },
                error: function(oResponse) {

                    messagesData.messages = [];

                    var sMessage = "";
                    sMessage = JSON.parse(oResponse.response.body).error.message.value;

                    var oMessageArray = [];
                    oMessageArray = sMessage.split("||");

                    messagesData.messagesLength = oMessageArray.length;
                    oMessageArray.forEach(function(item){
                      messagesData.messages.push({
                        type: 'Error',
                        title: item
                      });
                    });

                    Messages.setData(messagesData);
                    self.getView().setModel(Messages, "Messages");
                    oErrorIcon.setVisible(true);
                    self.getView().setBusy(false);

                }
            });

        },

        _createProductsModel: function() {
            var oData = new JSONModel({});

            this.getView().setModel("productsModel", oData);
        },

        getUserNameData: function() {

            var xmlHttp = null;

            xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    var oUserData = JSON.parse(xmlHttp.responseText);

                    userName = oUserData.id;
                }
            };
            xmlHttp.open("GET", this.userUrl, false);
            xmlHttp.send(null);

        },

        setProductCatsTable: function() {

            this.getView().byId("productTable").setModel(prodCatsModel, "prodCatsModel");

        },

        setProductCatsTree: function() {

            this.getView().byId("productTree").setModel(productTreeModel, "productTreeModel");

        },

        setTables: function() {

            this.getView().byId("commsTable").setModel(masterDetailsModel, "masterDetailsModel");
            this.getView().byId("faxTable").setModel(masterDetailsModel, "masterDetailsModel");
            this.getView().byId("emailTable").setModel(masterDetailsModel, "masterDetailsModel");
            this.getView().byId("taxTable").setModel(masterDetailsModel, "masterDetailsModel");

        },

        setCountries: function() {
            countriesModel.setSizeLimit(countriesModel.getData().results.length);
            this.getView().byId("addressCountry").setModel(countriesModel, "countriesModel");
            this.getView().byId("otherCountry").setModel(countriesModel, "countriesModel");
            this.getView().byId("ins-tel-country").setModel(countriesModel, "countriesModel");
            this.getView().byId("ins-fax-country").setModel(countriesModel, "countriesModel");
            //this.getView().byId("language-selection").setModel(countriesModel, "countriesModel");

        },

        setRegions: function(bReset) {
            if (!countryRegionsModel) countryRegionsModel = new JSONModel;
            var sCountryKey = this.getView().byId("addressCountry").getSelectedKey();
            var oRelatedRegions = { results : [] };

            oRelatedRegions.results = $.grep(regionsModel.oData.results, function(oRegion, index) {
                return oRegion.SubKey == sCountryKey || oRegion.SubKey == "";
            });
            countryRegionsModel.setData(oRelatedRegions);
            countryRegionsModel.setSizeLimit(oRelatedRegions.results.length+1);

            this.getView().byId("address-region").setModel(countryRegionsModel, "countryRegionsModel");

            //reset selected region on Country change
            if (bReset) this.getView().getModel().setProperty("/Region","")
        },

        changeRegions: function() {
            this.setRegions(true);
        },

        setTax: function() {

            taxTypeModel.setSizeLimit(taxTypeModel.getData().results.length+1);
            this.getView().byId("ins-tax-code-select").setModel(taxTypeModel, "taxTypeModel");

        },

        setCurrency: function() {

            currencyTypeModel.setSizeLimit(currencyTypeModel.getData().results.length+1);
            this.getView().byId("ins-currency-code-select").setModel(currencyTypeModel, "currencyTypeModel");

        },

        setMasterData: function() {

            this.getView().byId("headerFormView").setModel(masterDetailsModel);
            this.getView().byId("addressForm").setModel(masterDetailsModel);
            this.getView().setModel(masterDetailsModel);
            this.setTables();

        },

        setEmployeeContactTable: function() {

            this.getView().byId("employeeContactTable").setModel(employeeModel, "employeeModel");

        },

        getDataPromise: function(serviceParam, readEntity) {
            var oController = this;
            return new Promise(function(resolve, reject) {
                serviceParam.read(readEntity, {
                    success: function(data) {
                        resolve(data);
                    },
                    error: function(err) {
                        oController.handleGetAjaxFail(err);
                        reject();
                    }
                });
            });
        },

        setUpdateDataPromise: function(serviceParam, dataParam, updateEntity) {
            var oController = this;
            return new Promise(function(resolve, reject) {
                serviceParam.create(updateEntity, dataParam, {
                    method: "POST",
                    success: function(data) {
                        resolve(data);
                    },
                    error: function(err) {
                        oController.handleGetAjaxFailDuplicate(err);
                        reject();
                    }
                });
            });
        },

        showSuccessOverlay: function() {

            var oController = this

            if (!this._dialog) {
                this._dialog = sap.ui.xmlfragment("srm_compData.view.success", this);
                this.getView().addDependent(this._dialog);
            }

            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._dialog);
            this._dialog.open();

            setTimeout(function() {
                oController._dialog.close();
            }, 4000);

            location.reload();
        },

        handleGetAjaxFail: function(err) {

            console.log("Error in catch  == " + err);

            if (!this._dialog) {
                this._dialog = sap.ui.xmlfragment("srm_compData.view.error", this);
                this.getView().addDependent(this._dialog);
            }

            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._dialog);
            this._dialog.open();

        },

        handleGetAjaxFailDuplicate: function(err) {

            console.log("Error in catch  == " + err);

            if (!this._dialog) {
                this._dialog = sap.ui.xmlfragment("srm_compData.view.error_duplicate", this);
                this.getView().addDependent(this._dialog);
            }

            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._dialog);
            this._dialog.open();

        },

        reFreshData: function() {
            location.reload();
        },

        onAfterRendering: function() {
            var headerForm = this.getView().byId("headerForm");
            headerForm.setVisible(showHeaderForm);
            comsTableModel = this.getView().byId("commsTable");
            faxTableModel = this.getView().byId("faxTable");
            emailTableModel = this.getView().byId("emailTable");
            taxTableModel = this.getView().byId("taxTable");
        },

        changeToEdit: function(){
            if(!objectControllerModel.getProperty("/Active")){
                objectControllerModel.setProperty("/Active", true);
                this.toggleView();
            }
        },

        changeToDisplay: function(){
            this.getView().setBusy(true);
            if(objectControllerModel.getProperty("/Active")){
                objectControllerModel.setProperty("/Active", false);
                this.toggleView();
            }
            this.loadCompanyDetails(this.sSupplierId);
         },

        toggleView: function() {
            var headerFormView = this.getView().byId("headerFormView");
            var headerForm = this.getView().byId("headerForm");

            showHeaderForm = !showHeaderForm;
            showHeaderView = !showHeaderView;

            headerFormView.setVisible(showHeaderView);
            headerForm.setVisible(showHeaderForm);
        },

        addTelInfo: function(oEvent) {
            var newTelObj, telVal, telExt, getCountry;
            var bundle = this.getView().getModel("i18n").getResourceBundle();

            telVal = this.getView().byId("ins-tel").getValue();
            telExt = this.getView().byId("ins-Ext").getValue();
            getCountry = this.getView().byId("ins-tel-country").mProperties.selectedKey;

            if(telVal != "" && getCountry !== ""){
                newTelObj = {
                    "Telephone": telVal,
                    "Extension": telExt,
                    "Country": getCountry
                };

                var newCommsContact = comsTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyPhone/results");

                newCommsContact.push(newTelObj);
                comsTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyPhone/results", newCommsContact);
                //comsTableModel.getModel("masterDetailsModel").updateBindings();

            } else {

                this.messageBox(bundle.getText("pleaseEnterPhoneCountry"), "error");

            }

        },

        delTelInfo: function(oEvent) {
            var currentComms = null;
            var indexToDel = null;
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            indexToDel = parseInt(oEvent.getParameter("id").split("-")[oEvent.getParameter("id").split("-").length - 1]);
            var deleleTelDialog = new Dialog({
                title: bundle.getText("Confirm"),
                type: 'Message',
                content: new Text({
                    text: bundle.getText("delPhone")
                }),
                beginButton: new Button({
                    text: bundle.getText("submit"),
                    press: function(oEvent) {


                        currentComms = comsTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyPhone/results");
                        currentComms.splice(indexToDel, 1);
                        comsTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyPhone/results", currentComms);
                        //comsTableModel.getModel("masterDetailsModel").updateBindings();

                        deleleTelDialog.close();
                    }
                }),
                endButton: new Button({
                    text: bundle.getText("Cancel"),
                    press: function() {
                        deleleTelDialog.close();
                    }
                }),
                afterClose: function() {

                    deleleTelDialog.destroy();
                }
            });

            deleleTelDialog.open();

        },

        addFaxInfo: function(oEvent) {
            var newFaxObj, faxVal, faxExt, faxCountry, newFaxEntry;

            faxVal = this.getView().byId("ins-fax").getValue();
            faxExt = this.getView().byId("ins-fax-ext").getValue();
            faxCountry = this.getView().byId("ins-fax-country").mProperties.selectedKey;
            var bundle = this.getView().getModel("i18n").getResourceBundle();


            if(faxVal != "" && faxCountry !== ""){
                newFaxObj = {
                    "Fax": faxVal,
                    "Extension": faxExt,
                    "Country": faxCountry
                };

                newFaxEntry = faxTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyFax/results");

                newFaxEntry.push(newFaxObj);
                faxTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyFax/results", newFaxEntry);

            } else {

                this.messageBox(bundle.getText("pleaseEnterFaxCountry"), "error");

            }

        },

        delFaxInfo: function(oEvent) {
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var currentFax = null;
            var indexToDel = null;
            indexToDel = parseInt(oEvent.getParameter("id").split("-")[oEvent.getParameter("id").split("-").length - 1]);
            var deleleFaxDialog = new Dialog({
                title: bundle.getText("Confirm"),
                type: 'Message',
                content: new Text({
                    text: bundle.getText("delFax")
                }),
                beginButton: new Button({
                    text: bundle.getText("submit"),
                    press: function(oEvent) {


                        currentFax = faxTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyFax/results");
                        currentFax.splice(indexToDel, 1);
                        faxTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyFax/results", currentFax);

                        deleleFaxDialog.close();
                    }
                }),
                endButton: new Button({
                    text: bundle.getText("Cancel"),
                    press: function() {
                        deleleFaxDialog.close();
                    }
                }),
                afterClose: function() {

                    deleleFaxDialog.destroy();
                }
            });

            deleleFaxDialog.open();

        },

        addEmailInfo: function(oEvent) {
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var newEmailObj, emailVal, newEmailEntry;
            emailVal = this.getView().byId("ins-email").getValue();
            var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
            if (!emailVal.match(rexMail) && (emailVal.length > 0)) {
                //throw new ValidateException("'" + oValue + "' is not a valid email address");
                //MessageBox("'" + oValue + "' is not a valid email address");
                //this.messageBox("'" + emailVal + "' is not a valid email address", "error");
                this.messageBox(bundle.getText("pleaseEnterEmail"), "error");
            }

            if (emailVal.match(rexMail)) {
                newEmailObj = {
                    "EMail": emailVal
                };

                newEmailEntry = emailTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyEmail/results");
                newEmailEntry.push(newEmailObj);
                emailTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyEmail/results", newEmailEntry);

            } else if (emailVal.length == 0) {

                this.messageBox(bundle.getText("pleaseEnterEmail"), "error");

            }

        },

        delEmailInfo: function(oEvent) {

            var currentEmail = null;
            var indexToDel = null;
            var bundle = this.getView().getModel("i18n").getResourceBundle();

            indexToDel = parseInt(oEvent.getParameter("id").split("-")[oEvent.getParameter("id").split("-").length - 1]);

            var deleleEmailDialog = new Dialog({
                title: bundle.getText("Confirm"),
                type: 'Message',
                content: new Text({
                    text: bundle.getText("delEmail")
                }),
                beginButton: new Button({
                    text: bundle.getText("submit"),
                    press: function(oEvent) {


                        currentEmail = faxTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyEmail/results");
                        currentEmail.splice(indexToDel, 1);
                        emailTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyEmail/results", currentEmail);

                        deleleEmailDialog.close();
                    }
                }),
                endButton: new Button({
                    text: bundle.getText("Cancel"),
                    press: function() {
                        deleleEmailDialog.close();
                    }
                }),
                afterClose: function() {

                    deleleEmailDialog.destroy();
                }
            });

            deleleEmailDialog.open();

        },

        addTaxInfo: function(oEvent) {
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var newTaxObj, taxTypeVal, taxNumberVal, newTaxEntry;

            taxTypeVal = this.getView().byId("ins-tax-code-select").getSelectedKey();
            taxNumberVal = this.getView().byId("ins-tax-number").getValue();

            if (taxTypeVal.length > 0 && taxNumberVal.length > 0) {
                newTaxObj = {
                    "TaxType": taxTypeVal,
                    "TaxNumber": taxNumberVal
                }

                newTaxEntry = emailTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyTax/results");
                newTaxEntry.push(newTaxObj);
                taxTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyTax/results", newTaxEntry);

            } else {

                this.messageBox(bundle.getText("pleaseEnterTax"), "error");

            }

        },

        delTaxInfo: function(oEvent) {
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var currentTax = null;
            var indexToDel = null;
            indexToDel = parseInt(oEvent.getParameter("id").split("-")[oEvent.getParameter("id").split("-").length - 1]);
            var deleleTaxDialog = new Dialog({
                title: bundle.getText("Confirm"),
                type: 'Message',
                content: new Text({
                    text: bundle.getText("delTax")
                }),
                beginButton: new Button({
                    text: bundle.getText("submit"),
                    press: function(oEvent) {


                        currentTax = taxTableModel.getModel("masterDetailsModel").getProperty("/NavCompanyTax/results");
                        currentTax.splice(indexToDel, 1);
                        taxTableModel.getModel("masterDetailsModel").setProperty("/NavCompanyTax/results", currentTax);

                        deleleTaxDialog.close();
                    }
                }),
                endButton: new Button({
                    text: bundle.getText("Cancel"),
                    press: function() {
                        deleleTaxDialog.close();
                    }
                }),
                afterClose: function() {

                    deleleTaxDialog.destroy();
                }
            });

            deleleTaxDialog.open();

        },

        onSearch: function(oEvent) {

            var searchVal = oEvent.getParameter("newValue");
            var treeFilter = new sap.ui.model.Filter("Text", sap.ui.model.FilterOperator.Contains, searchVal);
            var oBinding = this.getView().byId("productTree").mBindingInfos.rows.binding;

            oBinding.filter(treeFilter, FilterType.Application);
            oBinding.expandToLevel(3);
        },

        addProductCategoryDialog: function(oEvent) {
            var bundle = this.getView().getModel("i18n").getResourceBundle();

            var oController = this;

            var prodTreeService = new sap.ui.model.odata.ODataModel(this.prodCatsUrl);
            var prodTreeEntity = "GetCategorySet?$filter=Language eq 'EN'";

            this.getDataPromise(prodTreeService, prodTreeEntity).then(function(data) {

                //GET PROD TREE MODEL
                productTreeModel = new JSONModel(data);
                oController.buildTreeStructure();
                oController.setProductCatsTree();

            }).catch(function(err) {

                oController.handleGetAjaxFail(err);

            });

            if (!this.oDialog) {
                // create dialog via fragment factory
                this.oDialog = sap.ui.xmlfragment(this.getView().getId(), "srm_compData.view.addProductCategoryDialog", this);


                this.oDialog.insertButton(new sap.m.Button("closeUploadDialog", {
                    text: bundle.getText("Cancel"),
                    enabled: true,
                    press: function() {
                        this.oDialog.close();
                    }.bind(this)
                }), 1);

                this.oDialog.insertButton(new sap.m.Button("addUploadDialog", {
                    text: bundle.getText("Add"),
                    enabled: true,
                    press: function(oEvent) {

                        oController.addCheckedItems();

                    }
                }), 2);

                this.getView().addDependent(this.oDialog);

            }

            this.oDialog.open();

        },

        addCheckedItems: function(oEvent, origProdTreeModel) {

            var treeProducts = this.getView().byId("productTree");
            var aSelectedInd = treeProducts.getSelectedIndices();
            var aExistingProducts = [];
            var oProductNewEntriesModel = prodCatsModel.getData();
            var oNewSelected = null;
            var bundle = this.getView().getModel("i18n").getResourceBundle();

            for (var i = 0; i < aSelectedInd.length; i++) {
                oNewSelected = treeProducts.getRows()[aSelectedInd[i]].getBindingContext("productTreeModel").getObject();

                if (oNewSelected.NodeIndicator !== "X" || oNewSelected.LevelIndicator === "04") {

                    oNewSelected.Status = bundle.getText("needToSubmit");
                    oNewSelected.SubmitStatus = true;
                    oNewSelected.Name = oNewSelected.Text;
                    oProductNewEntriesModel.results.unshift(oNewSelected);

                } else if (oNewSelected.NodeIndicator == "X") {

                    var errorParentProductCatDialog = new Dialog({

                        title: 'Error',
                        type: 'Message',
                        content: new Text({
                            text: 'You cannot add parent categories, please select child products'
                        }),
                        beginButton: new Button({
                            text: 'OK',
                            press: function(oEvent) {
                                errorParentProductCatDialog.close();
                            }
                        })
                    })
                    errorParentProductCatDialog.open();


                }

            }

            prodCatsModel.setData(oProductNewEntriesModel);
            this.setProductCatsTable();
            aExistingProducts.length = 0;

            this.oDialog.close();
        },

        buildTreeStructure: function() {

            var results = productTreeModel.getData().results;
            origProdTreeModel = new JSONModel(results);
            prodCatCount = results.length;

            for (var delKey in results) {

                delete results[delKey]["__metadata"]
            }

            var level1Arr = results.filter(function(keyItem) {
                return keyItem.LevelIndicator === "01"
            });
            var level2Arr = results.filter(function(keyItem) {
                return keyItem.LevelIndicator === "02"
            });
            var level3Arr = results.filter(function(keyItem) {
                return keyItem.LevelIndicator === "03"
            });
            var level4Arr = results.filter(function(keyItem) {
                return keyItem.LevelIndicator === "04"
            });
            var level5Arr = results.filter(function(keyItem) {
                return keyItem.LevelIndicator === "05"
            });

            for (var keyLevel1 in level1Arr) {

                level1Arr[keyLevel1].categories = [];

                for (var keyLevel2 in level2Arr) {

                    if (level2Arr[keyLevel2].ParentGuid === level1Arr[keyLevel1].SpcGuid) {

                        level1Arr[keyLevel1].categories.push(level2Arr[keyLevel2]);

                    }

                }
            }

            for (var keyLevel2 in level2Arr) {

                for (var keyLevel3 in level3Arr) {

                    if (level3Arr[keyLevel3].ParentGuid === level2Arr[keyLevel2].SpcGuid) {

                        level2Arr[keyLevel2].categories = [];
                        level2Arr[keyLevel2].categories.push(level3Arr[keyLevel3]);

                    }

                }

            }

            for (var keyLevel3 in level3Arr) {

                level3Arr[keyLevel3].categories = [];

                for (var keyLevel4 in level4Arr) {

                    if (level4Arr[keyLevel4].ParentGuid === level3Arr[keyLevel3].SpcGuid) {

                        level3Arr[keyLevel3].categories.push(level4Arr[keyLevel4]);

                    }

                }

            }


            //      for (var keyLevel4 in level4Arr) {

            //        level4Arr[keyLevel4].categories = [];

            //        for (var keyLevel5 in level5Arr) {

            //          if (level5Arr[keyLevel5].ParentGuid === level4Arr[keyLevel4].SpcGuid) {

            //            level4Arr[keyLevel4].categories.push(level5Arr[keyLevel5]);

            //          }

            //        }

            //      }

            //        Object.values(productTreeModel.oData.results.map(function (item) {
            //          item.categories = productTreeModel.oData.results.filter(child => child.ParentGuid === item.SpcGuid);
            //          return item;
            //        }).sort(
            //          function(itema, itemb) {
            //            parseInt(itemb.LevelIndicator) - parseInt(itema.LevelIndicator)
            //        }).reduce(
            //          function(result, item) {
            //            item.categories = item.categories.map(child => {
            //              if (result[child.SpcGuid]) {
            //              return result[child.SpcGuid];
            //            }
            //              return child;
            //          });
            //          result[item.SpcGuid] = item;
            //          return result;
            //        },
            //      { })).filter(function(item){ item.LevelIndicator === "01"});

            productTreeModel.oData.results.length = 0;
            productTreeModel.oData.results = level1Arr;

        },

        submitProductInfo: function(oEvent) {

            var oController = this;
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var addProductCatDialog = new Dialog({

                title: bundle.getText("Confirm"),
                type: 'Message',

                content: new Text({
                    text: bundle.getText("saveChangesProduct")
                }),
                beginButton: new Button({
                    text: bundle.getText("submit"),
                    press: function(oEvent) {

                        var postCatsModel = prodCatsModel.getData().results;
                        var udateService = new sap.ui.model.odata.ODataModel(oController.prodCatsUrl);
                        var updateEntity = "AddCategorySet";
                        var postObject = {
                            "Supplier": supplierId,
                            "NavAddCategorySet": []
                        };
                        var entry = null;

                        var filteredPostCatModel = postCatsModel.filter(function(keyItem) {
                            return (keyItem["SubmitStatus"])
                        });

                        for (var key in filteredPostCatModel) {

                            var singleEntryObj = {
                                "Name": "",
                                "CategoryGuid": "",
                                "Parent": "",
                                "Status": "",
                                "Visibility": ""
                            };

                            singleEntryObj.Name = filteredPostCatModel[key].Name;

                            if (filteredPostCatModel[key].DeleteIcon === "X") {
                                singleEntryObj.Status = "M";
                                singleEntryObj.CategoryGuid = filteredPostCatModel[key].CategoryGuid;
                                singleEntryObj.Parent = filteredPostCatModel[key].Parent;
                            } else {
                                singleEntryObj.Status = "A";
                                singleEntryObj.CategoryGuid = filteredPostCatModel[key].SpcGuid;
                                singleEntryObj.Parent = filteredPostCatModel[key].ParentGuid;
                            }
                            singleEntryObj.Visibility = "X";
                            postObject.NavAddCategorySet[key] = singleEntryObj;

                        }

                        oController.setUpdateDataPromise(udateService, postObject, updateEntity).then(function(data) {

                            addProductCatDialog.close();
                            oController.showSuccessOverlay();
                            //obj.results = restetPostCatModel;
                            //postCatsModel.setData(postCatsModel);
                            //prodCatsModel.oData.results = restetPostCatModel;
                            //this.setProductCatsTable();

                        }).catch(function(err) {

                            addProductCatDialog.close()
                            oController.handleGetAjaxFailDuplicate(err);

                        });

                    }
                }),
                endButton: new Button({
                    text: bundle.getText("Cancel"),
                    press: function() {
                        addProductCatDialog.close();
                    }
                }),
                afterClose: function() {

                    addProductCatDialog.destroy();
                }
            });

            addProductCatDialog.open();

        },

        resetProductInfo: function(oEvent) {

            var postCatsModel = prodCatsModel.getData().results;
            var obj = {};

            var restetPostCatModel = postCatsModel.filter(function(keyItem) {
                return (!keyItem["SubmitStatus"])
            });
            obj.results = restetPostCatModel;
            prodCatsModel.setData(obj);
            //prodCatsModel.oData.results = restetPostCatModel;
            this.setProductCatsTable();
            //prodCatsModel.setProductCatsTable();

        },

        delProductCatInfo: function(oEvent) {
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var rowIndex = oEvent.getSource().getId();
            rowIndex = rowIndex.substr(44, 2);
            var rowIndexNum = parseInt(rowIndex);
            var bundle = this.getView().getModel("i18n").getResourceBundle();
            var postCatsModel = prodCatsModel.getData().results;
            var currentProductCatInfo = null;
            var deleteProductCatInfo = [];
            var indexToDel = null;
            var toDelete = null;
            var deleleProductCatDialog = new Dialog({
                title: bundle.getText("Confirm"),
                type: 'Message',
                content: new Text({
                    text: bundle.getText("delProduct")
                }),
                beginButton: new Button({
                    text: bundle.getText("Submit"),
                    press: function(oEvent) {

                        //indexToDel = parseInt(oEvent.getParameter("id").split("-")[oEvent.getParameter("id").split("-").length -1]);
                        currentProductCatInfo = prodCatsModel.getProperty("/results");
                        deleteProductCatInfo = prodCatsModel.getProperty("/results");
                        toDelete = deleteProductCatInfo.splice(rowIndexNum, 1);
                        //currentProductCatInfo.splice(rowIndexNum, 1);
                        toDelete[0].SubmitStatus = true;
                        toDelete[0].Status = bundle.getText("needToSubmit");
                        prodCatsModel.setProperty("/results", deleteProductCatInfo);
                        postCatsModel.push(toDelete[0]);
                        deleleProductCatDialog.close();
                    }
                }),
                endButton: new Button({
                    text: bundle.getText("Cancel"),
                    press: function() {
                        deleleProductCatDialog.close();
                    }
                }),
                afterClose: function() {

                    deleleProductCatDialog.destroy();
                }
            });

            deleleProductCatDialog.open();

        },
        messageBox: function(msg, type) {
            var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
            if (type == "error") {
                MessageBox.error(
                    msg, {
                        styleClass: bCompact ? "sapUiSizeCompact" : ""
                    });
            } else {
                MessageBox.success(
                    msg, {
                        styleClass: bCompact ? "sapUiSizeCompact" : ""
                    });
            }
        },

        setProfileImage: function(){

          var oView = this.getView();
          var oPageHeader = oView.byId("headerForTest");
          var date = new Date();

          var sPath = this.servUrl +
                      "DownloadSet(Supplier='" +
                      this.sSupplierId  +
                      "')/$value?" +
                      date.getTime();

         oPageHeader.setObjectImageURI(sPath);

        },

        handleUploadPress: function(){

          var that = this;

          if(this.FileName && this.FileType){

            var oView = this.getView();
            oView.setBusy(true);

            var oDataModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/BSHD/SU_MSTR_DATA_SRV;mo/");
            var sToken = oDataModel.getSecurityToken();

            var oFileUploader = oView.byId("profile-image");
            oFileUploader.removeAllHeaderParameters();

            oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
                 name: "x-csrf-token",
                 value: sToken
             }));

            oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
                 name: "Content-Type",
                 value: that.FileType
            }));

            oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
                 name: "SLUG",
                 value: that.sSupplierId +
                        "::A::" +
                        that.FileName +
                        //"::DOCUMENTATION"
                        "::Supplier-Uploaded Documents"
            }));

            oFileUploader.upload();
          }

        },

        handleValueChange: function(oEvent){
          var files = oEvent.getParameter("files");
          if (files.length === 1) {
             this.FileName = files[0].name;
             this.FileType = files[0].type;
          }else{
             this.FileName = "";
             this.FileType = "";
          }
        },

        handleUploadComplete: function(oResponse){

          this.FileName = "";
          this.FileType = "";

          var messagetype = oResponse.getParameters().headers.messagetype;
          var message = oResponse.getParameters().headers.message;

          setTimeout(function(){
            this.getView().setBusy(false);

            if(messagetype == "I"){
              var txt = this.getView().getModel("i18n").getResourceBundle().getText("uploaded");
              MessageBox.information(txt);
            }else{
              MessageBox.error(message);
            }

            this.setProfileImage();
          }.bind(this), 1000);

        },

        handleTypeMissmatch: function(oEvent){
          var txt = this.getView().getModel("i18n").getResourceBundle().getText("wrongTypeFile");
          txt = txt.replace("&1", oEvent.getParameters().fileType);
          MessageBox.error(txt);
        },

        handleMessagePopoverPress: function (oEvent) {
          oMessagePopover.toggle(oEvent.getSource());
        }
    });
});