sap.ui.define(['sap/ui/model/SimpleType','sap/ui/model/ValidateException',"sap/m/MessageBox"], function (SimpleType,ValidateException,MessageBox) {
  "use strict";
  return {
    dateFormatter: function (sDate) {


      if (sDate !=  null) {

        var year = sDate.slice(0,4);
        var month = sDate.slice(4,6);
        var day = sDate.slice(6,8);

        var date = day + "/" + month + "/" + year;

        return date;
      }

      return sDate;

    },

    getCountryName: function(countryModel){

    },
    dateTimeFormat: function (dateTime) {

      if (dateTime != "null") {
        var date = new Date(dateTime);
        return date.getDay() + "/" + date.getMonth() + "/" + date.getYear();
      }

      return dateTime;

    },
     showDelete: function(status){
     var bundle = this.getView().getModel("i18n").getResourceBundle();


      if (status == bundle.getText("Updated")){
        return true;
      }
      return false;
      //return !!parent;
    },

    toInteger: function(sStringValue){

      return parseInt(sStringValue);

    }


  };
});